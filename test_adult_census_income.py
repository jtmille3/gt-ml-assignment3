import numpy as np
import pandas as pd

from sklearn import preprocessing
from sklearn import impute
from sklearn import cluster
from sklearn import mixture
from sklearn import neural_network
from sklearn import decomposition
from sklearn import random_projection
from sklearn import discriminant_analysis
from sklearn.utils import resample

from assignment3 import run
from assignment3 import __globals__


def preprocess_transform(df):
    # Remove rows with "?"
    # https://archive.ics.uci.edu/ml/datasets/adult
    # https://github.com/anyarcherty/Adult-Census-Income/blob/master/census-income.names
    for column in df.columns:
        if df[column].dtype == np.object:
            df[column][df[column] == "?"] = np.nan

    df.dropna(inplace=True, axis='index')

    # https://elitedatascience.com/imbalanced-classes
    # Upsample
    df_majority = df[df.income == '<=50K']
    df_minority = df[df.income == '>50K']

    # downsample minority class
    df_majority_downsampled = resample(df_majority,
                                     replace=True,  # sample with replacement
                                     n_samples=df_minority.shape[0],  # to match minority class
                                     random_state=42)  # reproducible results

    # Combine majority class with upsampled minority class
    df_downsampled = pd.concat([df_majority_downsampled, df_minority])

    df.drop(df.index, inplace=True)
    df.loc[:, :] = df_downsampled.values

    df.income[df.income == '<=50K'] = 1
    df.income[df.income == '>50K'] = 0

    imputer = impute.SimpleImputer(missing_values=np.nan, strategy="constant", fill_value=None)
    df = pd.DataFrame(imputer.fit_transform(df), columns=df.columns)

    # Convert categoricals to ordinals
    ordinal = preprocessing.OrdinalEncoder(categories="auto")
    df = pd.DataFrame(ordinal.fit_transform(df), columns=df.columns)

    # Bin all values to remove variance in regression values
    discretizer = preprocessing.KBinsDiscretizer(n_bins=101, encode="ordinal", strategy="uniform")
    df = pd.DataFrame(discretizer.fit_transform(df), columns=df.columns)

    df.income[df.income == 100.0] = 1
    df.income[df.income == 0.0] = 0

    return df


def test_adult_census_income():
    __globals__["save"] = True
    __globals__["reset"] = False
    __globals__["grid_search"] = False

    neural_network_classifier_fixture = {
        'title': 'Neural Network',
        'func': neural_network.MLPClassifier,
        'init': {
            'activation': 'tanh',
            'hidden_layer_sizes': 140,
            'solver': 'adam',
            'beta_1': 0.85,
            'beta_2': 0.99,
            'epsilon': 1e-6,
            'early_stopping': True
        },
        'grid_search': {
            'estimator__activation': ['identity', 'logistic', 'tanh', 'relu'],
            'estimator__hidden_layer_sizes': list(range(20, 200, 20)),
            'estimator__beta_1': np.linspace(0.85, 0.95, 3),  # default is 0.9
            'estimator__beta_2': np.linspace(0.99, 0.9999, 3),  # default is 0.999
            'estimator__epsilon': np.linspace(1e-5, 1e-9, 5),
            'estimator__early_stopping': [True, False]
        }
    }

    kmeans_cluster_fixture = {
        'title': 'K-Means Clustering',
        'func': cluster.KMeans,
        'init': {
            'n_clusters': 2,
            'n_init': 10,
            'max_iter': 200
        }, 'grid_search': {
            'estimator__n_clusters': list(range(2, 16, 2)),
            'estimator__n_init': list(range(2, 20, 2)),
            'estimator__max_iter': list(range(200, 700, 100))
        }
    }

    em_cluster_fixture = {
        'title': 'Expectation Maximization',
        'func': mixture.GaussianMixture,
        'init': {
            'n_components': 2,
            'max_iter': 200,
            'n_init': 10,
            'covariance_type': 'full'
        }, 'grid_search': {
            'estimator__n_components': list(range(1, 11, 1)),
            'estimator__max_iter': list(range(100, 600, 100)),
            'estimator__covariance_type': ['full', 'tied', 'diag', 'spherical']
        }
    }

    pca_reduction_fixture = {
        'title': 'Principal Component Analysis',
        'func': decomposition.PCA,
        'init': {
            'n_components': 2
        }
    }

    ica_reduction_fixture = {
        'title': 'Independent Component Analysis',
        'func': decomposition.FastICA,
        'init': {
            'n_components': 11
        }
    }

    rcp_reduction_fixture = {
        'title': 'Random Component Analysis',
        'func': random_projection.GaussianRandomProjection,
        'init': {
            'n_components': 8
        }
    }

    lda_reduction_fixture = {
        'title': 'Linear Discriminant Analysis',
        'func': discriminant_analysis.LinearDiscriminantAnalysis,
        'init': {
            'n_components': 1
        }
    }

    run(
        path="adult-census-income",
        target="income",
        separator=",",
        preprocess=preprocess_transform,
        supervised_fixtures=[
            neural_network_classifier_fixture
        ],
        unsupervised_fixtures=[
            kmeans_cluster_fixture,
            em_cluster_fixture
        ],
        reduction_fixtures=[
            pca_reduction_fixture,
            ica_reduction_fixture,
            rcp_reduction_fixture,
            lda_reduction_fixture
        ])