import pickle
import os
import time

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from numpy import linalg

from sklearn import metrics

from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline

from sklearn import decomposition
from sklearn import random_projection
from sklearn import discriminant_analysis

import warnings
warnings.filterwarnings("ignore")

__globals__ = {
    "logger": None,
    "cv": StratifiedKFold(n_splits=5, shuffle=True, random_state=42),
    # "cv": RepeatedStratifiedKFold(n_splits=5, n_repeats=3, random_state=42),
    "scorer": metrics.log_loss,
    "save": False,
    "reset": False,
    "grid_search": True,
}


def open_log(path, file):
    # Remove all previous results
    if not os.path.exists("results/" + path):
        os.mkdir("results/" + path)
    elif os.path.exists("results/" + path + "/" + file):
        os.remove("results/" + path + "/" + file)

    __globals__["logger"] = open("results/" + path + "/" + file, "w")


def close_log():
    __globals__["logger"].flush()
    __globals__["logger"].close()


def log(text):
    if __globals__["save"]:
        __globals__["logger"].write(str(text) + "\n")
        __globals__["logger"].flush()
    else:
        log(text)


def load_datasets(path, target, separator=",", reset=False, preprocess=None):
    path = "datasets/" + path
    if not reset and os.path.isfile(path + ".pkl"):
        return pickle.load(open(path + ".pkl", "rb"))
    else:
        df = pd.read_csv(path + ".csv", sep=separator)

        if preprocess:
            df = preprocess(df)

        X = df.loc[:, df.columns != target]
        y = df.loc[:, df.columns == target]

        X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=0.75, random_state=42, stratify=y)

        y_distinct_values, y_distinct_counts = np.unique(y.values, return_counts=True)

        for i in list(range(len(y_distinct_values))):
            log("y: {} observations of the value ''{}''".format(y_distinct_counts[i], y_distinct_values[i]))

        y_train_distinct_values, y_train_distinct_counts = np.unique(y_train.values, return_counts=True)

        for i in list(range(len(y_train_distinct_values))):
            log("Training(y): {} observations of the value ''{}''".format(y_train_distinct_counts[i], y_train_distinct_values[i]))

        y_test_distinct_values, y_test_distinct_counts = np.unique(y_test.values, return_counts=True)

        for i in list(range(len(y_test_distinct_values))):
            log("Testing(y): {} observations of the value ''{}''".format(y_test_distinct_counts[i], y_test_distinct_values[i]))

        log("")

        datasets = {
            'X_train': X_train,
            'y_train': y_train.values.flatten(),
            'X_test': X_test,
            'y_test': y_test.values.flatten()
        }

        pickle.dump(datasets, open(path + ".pkl", "wb"))

        return datasets


def timeit(func, *args, **kwargs):
    # https://stackoverflow.com/questions/24812253/how-can-i-capture-return-value-with-python-timeit-module#
    start_time = time.time()
    value = func(*args, **kwargs)
    end_time = time.time()
    return end_time - start_time, value


def format_time(seconds):
    millis = int(round(seconds * 1000))
    seconds = int(seconds)
    minutes = (seconds / 60) % 60
    minutes = int(minutes)
    hours = (seconds / (60 * 60)) % 24
    return "%02d:%02d:%02d.%03d" % (hours, minutes, seconds, millis)


def report_unsupervised(unsupervised_fixture, datasets, path):
    log("===============================================================")
    log("")

    unsupervised_title = unsupervised_fixture['title']
    unsupervised_func = unsupervised_fixture['func']
    unsupervised_init = unsupervised_fixture['init']

    log(unsupervised_title)

    X_train = datasets['X_train']
    y_train = datasets['y_train']
    X_test = datasets['X_test']
    y_test = datasets['y_test']

    samples, features = X_train.shape

    cluster = unsupervised_func(**unsupervised_init)

    fit_elapsed, cluster = timeit(cluster.fit, X_train)

    log("Statistics for the best {} estimator".format(unsupervised_title))
    log("Training fit time: {}".format(format_time(fit_elapsed)))
    log("")

    train_predict_elapsed, y_train_pred = timeit(cluster.predict, X_train)

    log("Training query time: {}".format(format_time(train_predict_elapsed)))
    # log("Train adjusted rand index: {}".format(metrics.adjusted_rand_score(y_train, y_train_pred)))
    log("Train mutual information: {}".format(metrics.adjusted_mutual_info_score(y_train, y_train_pred)))
    # log("Train homogeneity: {}".format(metrics.homogeneity_score(y_train, y_train_pred)))
    # log("Train completeness: {}".format(metrics.completeness_score(y_train, y_train_pred)))
    # log("Train v-measure: {}".format(metrics.v_measure_score(y_train, y_train_pred)))
    # log("Train fowlkes mallows: {}".format(metrics.fowlkes_mallows_score(y_train, y_train_pred)))

    log("Train silhouette: {}".format(metrics.silhouette_score(X_train, y_train_pred)))

    log("")

    if features > 3:
        features = 3

    offset = 0

    fig, ax = plt.subplots(features, features)

    fig.suptitle(unsupervised_title)

    min = X_train.values.min()
    max = X_train.values.max()

    if hasattr(cluster, 'cluster_centers_'):
        centers = cluster.cluster_centers_
    else:
        centers = cluster.means_

    for x in range(0, features):
        for y in range(0, features):
            ax[x, y].set_xlim([min, max])
            ax[x, y].set_ylim([min, max])
            if x == y:
                ax[x, y].text(max / 2, max / 2, 'Feature %d' % (x + offset + 1), horizontalalignment='center', verticalalignment='center', fontdict={'size': 10})
            else:
                ax[x, y].scatter(X_train.values[:, x + offset], X_train.values[:, y + offset], c=y_train_pred)
                ax[x, y].scatter(centers[:, x + offset], centers[:, y + offset], c='black', s=200, alpha=0.5)
                ax[x, y].grid()

    if __globals__["save"]:
        plt.savefig('results/%s/Cluster - %s Training.png' % (path, unsupervised_title))
    else:
        plt.show()

    plt.close()

    test_predict_elapsed, y_test_pred = timeit(cluster.predict, X_test)

    log("Test query time: {}".format(format_time(test_predict_elapsed)))
    # log("Test adjusted rand index: {}".format(metrics.adjusted_rand_score(y_test, y_test_pred)))
    log("Test mutual information: {}".format(metrics.adjusted_mutual_info_score(y_test, y_test_pred)))
    # log("Test homogeneity: {}".format(metrics.homogeneity_score(y_test, y_test_pred)))
    # log("Test completeness: {}".format(metrics.completeness_score(y_test, y_test_pred)))
    # log("Test v-measure: {}".format(metrics.v_measure_score(y_test, y_test_pred)))
    # log("Test fowlkes mallows: {}".format(metrics.fowlkes_mallows_score(y_test, y_test_pred)))

    log("Test silhouette: {}".format(metrics.silhouette_score(X_test, y_test_pred)))

    log("")

    # fig, ax = plt.subplots(features, features)
    #
    # fig.suptitle(unsupervised_title)
    #
    # min = X_test.values.min()
    # max = X_test.values.max()
    #
    # if hasattr(cluster, 'cluster_centers_'):
    #     centers = cluster.cluster_centers_
    # else:
    #     centers = cluster.means_
    #
    # for x in range(0, features):
    #     for y in range(0, features):
    #         ax[x, y].set_xlim([min, max])
    #         ax[x, y].set_ylim([min, max])
    #         if x == y:
    #             ax[x, y].text(max / 2, max / 2, 'Feature %d' % (x + + offset + 1), horizontalalignment='center', verticalalignment='center', fontdict={'size': 10})
    #         else:
    #             ax[x, y].scatter(X_train.values[:, x + offset], X_train.values[:, y + offset], c=y_train_pred)
    #             ax[x, y].scatter(centers[:, x + offset], centers[:, y + offset], c='black', s=200, alpha=0.5)
    #             ax[x, y].grid()
    #
    # if __globals__["save"]:
    #     plt.savefig('results/%s/Cluster - %s Testing.png' % (path, unsupervised_title))
    # else:
    #     plt.show()
    #
    # plt.close()

    score = []
    inter = []
    intra = []
    for k in range(2, features + 1):
        if hasattr(cluster, 'n_clusters'):
            cluster.set_params(n_clusters=k)
        else:
            cluster.set_params(n_components=k)

        cluster.fit(X_train)

        if hasattr(cluster, 'cluster_centers_'):
            centers = cluster.cluster_centers_
        else:
            centers = cluster.means_

        y_train_pred = cluster.predict(X_train)
        inter.append(centers.mean())
        intra.append(metrics.silhouette_samples(X_train, y_train_pred, 'euclidean').mean())

        if hasattr(cluster, 'bic'):
            score.append(cluster.bic(X_train))
        else:
            score.append(cluster.score(X_train))

    plt.plot(range(2, features + 1), inter)
    plt.title('%s Inter Cluster Means' % unsupervised_title)
    plt.xlabel('# of Clusters')
    plt.ylabel('Inter Cluster Mean')
    plt.grid(True)
    plt.xticks(range(2, features + 1), rotation=45)

    if __globals__["save"]:
        plt.savefig('results/%s/Cluster - %s Inter Cluster Means.png' % (path, unsupervised_title))
    else:
        plt.show()

    plt.close()

    plt.plot(range(2, features + 1), intra)
    plt.title('%s Intra Cluster Means' % unsupervised_title)
    plt.xlabel('# of Clusters')
    plt.ylabel('Intra Cluster Mean')
    plt.grid(True)
    plt.xticks(range(2, features + 1), rotation=45)

    if __globals__["save"]:
        plt.savefig('results/%s/Cluster - %s Intra Cluster Means.png' % (path, unsupervised_title))
    else:
        plt.show()

    plt.close()

    plt.plot(range(2, features + 1), intra)
    plt.title('%s Scores' % unsupervised_title)
    plt.xlabel('# of Clusters')
    plt.ylabel('Score')
    plt.grid(True)
    plt.xticks(range(2, features + 1), rotation=45)

    if __globals__["save"]:
        plt.savefig('results/%s/Cluster - %s Score.png' % (path, unsupervised_title))
    else:
        plt.show()

    plt.close()


def report_supervised(supervised_fixture, datasets):
    log("===============================================================")
    log("")

    X_train = datasets['X_train']
    y_train = datasets['y_train']
    X_test = datasets['X_test']
    y_test = datasets['y_test']

    supervised_title = supervised_fixture['title']
    supervised_func = supervised_fixture['func']
    supervised_init = supervised_fixture['init']

    log('Statistics for %s' % supervised_title)

    classifier = supervised_func(**supervised_init)

    fit_elapsed, classifier = timeit(classifier.fit, X_train, y_train)

    log("Training fit time: {}".format(format_time(fit_elapsed)))
    log("")

    train_predict_elapsed, y_train_pred = timeit(classifier.predict, X_train)

    log("Training query time: {}".format(format_time(train_predict_elapsed)))
    log("Train accuracy: {}".format(metrics.accuracy_score(y_train, y_train_pred)))
    log("Train mse: {}".format(metrics.mean_squared_error(y_train, y_train_pred)))
    log("")

    test_predict_elapsed, y_test_pred = timeit(classifier.predict, X_test)

    log("Test query time: {}".format(format_time(test_predict_elapsed)))
    log("Test accuracy: {}".format(metrics.accuracy_score(y_test, y_test_pred)))
    log("Test mse: {}".format(metrics.mean_squared_error(y_test, y_test_pred)))
    log("")


def report_dimensionality_reduction(reduction_fixture, datasets, path):
    log("===============================================================")
    log("")

    if reduction_fixture['func'] == decomposition.PCA:
        report_pca(reduction_fixture, datasets, path)
    elif reduction_fixture['func'] == decomposition.FastICA:
        report_ica(reduction_fixture, datasets, path)
    elif reduction_fixture['func'] == random_projection.GaussianRandomProjection:
        report_rca(reduction_fixture, datasets, path)
    elif reduction_fixture['func'] == discriminant_analysis.LinearDiscriminantAnalysis:
        report_lda(reduction_fixture, datasets, path)


def dimension_scatter(reduction_fixture, datasets, path):
    X_train = datasets['X_train']
    y_train = datasets['y_train']

    reduction_title = reduction_fixture['title']
    reduction_func = reduction_fixture['func']
    reduction_init = reduction_fixture['init']

    samples, features = X_train.shape
    reduction = reduction_func(**reduction_init)
    X_reduced = reduction.fit_transform(X_train, y_train)
    reduced_samples, reduced_features = X_reduced.shape

    # http://scikit-learn.org/stable/auto_examples/preprocessing/plot_scaling_importance.html#sphx-glr-auto-examples-preprocessing-plot-scaling-importance-py
    y_train_unique = np.unique(y_train)
    fig, ax = plt.subplots(reduced_features, reduced_features)

    fig.suptitle(reduction_title)

    min = X_reduced.min()
    max = X_reduced.max()

    for x in range(0, reduced_features):
        for y in range(0, reduced_features):
            ax[x, y].set_xlim([min, max])
            ax[x, y].set_ylim([min, max])
            for l in y_train_unique:
                if x == y:
                    ax[x, y].text(0, 0, 'PC%d' % (x + 1), horizontalalignment='center', verticalalignment='center', fontdict={'size': 20})
                else:
                    ax[x, y].scatter(X_reduced[y_train == l, x], X_reduced[y_train == l, y])
                    ax[x, y].grid()

    # ax1.set_title('Training dataset after PCA')
    # ax1.set_xlabel('1st principal component')
    # ax1.set_ylabel('2nd principal component')
    # ax1.legend(loc='upper right')

    if __globals__["save"]:
        plt.savefig('results/%s/%s - Scatter All.png' % (path, reduction_title))
    else:
        plt.show()

    plt.close()

    fig, ax = plt.subplots(ncols=1)

    fig.suptitle(reduction_title)

    for l in y_train_unique:
        ax.scatter(X_reduced[y_train == l, 0], X_reduced[y_train == l, 1])
        ax.grid()

    if __globals__["save"]:
        plt.savefig('results/%s/%s - Scatter DIM 1 & 2.png' % (path, reduction_title))
    else:
        plt.show()

    plt.close()


def report_pca(reduction_fixture, datasets, path):
    X_train = datasets['X_train']
    y_train = datasets['y_train']

    reduction_title = reduction_fixture['title']
    reduction_func = reduction_fixture['func']
    reduction_init = reduction_fixture['init']

    dimension_scatter(reduction_fixture, datasets, path)

    samples, features = X_train.shape
    reduction = reduction_func(**reduction_init)
    reduction.set_params(n_components=features)
    X_train_reduced = reduction.fit_transform(X_train, y_train)

    log("PCA reduced features to %d" % X_train_reduced.shape[1])

    variance = []
    for component in range(1, features + 1):
        variance.append(reduction.explained_variance_ratio_[0:component].sum())

    plt.plot(range(1, features + 1), variance)
    plt.title('%s Cumulative Variance' % reduction_title)
    plt.xlabel('# of Components')
    plt.ylabel('Cumulative Variance')
    plt.grid(True)
    plt.xticks(range(1, features + 1), rotation=45)

    if __globals__["save"]:
        plt.savefig('results/%s/%s - Cumulative Variance.png' % (path, reduction_title))
    else:
        plt.show()

    plt.close()

    plt.plot(range(1, len(reduction.explained_variance_ratio_) + 1), reduction.explained_variance_ratio_)
    plt.title('%s Variance Ratio' % reduction_title)
    plt.xlabel('Components')
    plt.ylabel('Variance Ratio')
    plt.grid(True)
    plt.xticks(range(1, len(reduction.explained_variance_ratio_) + 1), rotation=45)

    if __globals__["save"]:
        plt.savefig('results/%s/%s - Variance Ratio.png' % (path, reduction_title))
    else:
        plt.show()

    plt.close()

    error = []
    # Measure the reconstruction error per components
    for component in range(1, features + 1):
        reduction.set_params(n_components=component)
        reduced = reduction.fit_transform(X_train, y_train)
        projected = reduction.inverse_transform(reduced)
        error.append(linalg.norm((X_train - projected), None))

    plt.plot(range(1, features + 1), error)
    plt.title('%s - Reconstruction Error' % reduction_title)
    plt.xlabel('# of Components')
    plt.ylabel('Error')
    plt.grid(True)
    plt.xticks(range(1, features + 1), rotation=45)

    if __globals__["save"]:
        plt.savefig('results/%s/%s - Reconstruction.png' % (path, reduction_title))
    else:
        plt.show()

    plt.close()


def report_ica(reduction_fixture, datasets, path):
    X_train = datasets['X_train']
    y_train = datasets['y_train']

    reduction_title = reduction_fixture['title']
    reduction_func = reduction_fixture['func']
    reduction_init = reduction_fixture['init']

    samples, features = X_train.shape
    reduction = reduction_func(**reduction_init)
    X_train_reduced = reduction.fit_transform(X_train, y_train)

    log("ICA reduced features to %d" % X_train_reduced.shape[1])

    # dimension_scatter(reduction_fixture, datasets, path)

    kurt = []
    for component in range(1, features + 1):
        reduction.set_params(n_components=component)
        tmp = reduction.fit_transform(X_train, y_train)
        tmp = pd.DataFrame(tmp)
        tmp = tmp.kurt(axis=0)
        kurt.append(tmp.abs().mean())

    plt.plot(range(1, features + 1), kurt)
    plt.title('%s Kurtosis' % reduction_title)
    plt.xlabel('# of Components')
    plt.ylabel('Kurtosis')
    plt.grid(True)
    plt.xticks(range(1, features + 1), rotation=45)

    if __globals__["save"]:
        plt.savefig('results/%s/%s - Reduction.png' % (path, reduction_title))
    else:
        plt.show()

    plt.close()


def report_rca(reduction_fixture, datasets, path):
    X_train = datasets['X_train']
    y_train = datasets['y_train']

    reduction_title = reduction_fixture['title']
    reduction_func = reduction_fixture['func']
    reduction_init = reduction_fixture['init']

    samples, features = X_train.shape
    reduction = reduction_func(**reduction_init)
    X_train_reduced = reduction.fit_transform(X_train, y_train)

    log("RCA reduced features to %d" % X_train_reduced.shape[1])

    # dimension_scatter(reduction_fixture, datasets, path)

    # Run it 5 times.
    for i in range(0, 10):
        error = []

        # Measure the reconstruction error per components
        for component in range(1, features + 1):
            reduction.set_params(n_components=component)
            reduced = reduction.fit_transform(X_train, y_train)
            reconstructed = reduced.dot(reduction.components_)
            error.append(linalg.norm(X_train - reconstructed))

        plt.plot(range(1, features + 1), error, label='Run %d' % (i + 1))

    plt.title('%s - Reconstruction Error (10 Runs)' % reduction_title)
    plt.xlabel('# of Components')
    plt.ylabel('Error')
    plt.grid(True)
    plt.xticks(range(1, features + 1), rotation=45)

    if __globals__["save"]:
        plt.savefig('results/%s/%s - Reconstruction.png' % (path, reduction_title))
    else:
        plt.show()

    plt.close()


def report_lda(reduction_fixture, datasets, path):
    X_train = datasets['X_train']
    y_train = datasets['y_train']

    reduction_title = reduction_fixture['title']
    reduction_func = reduction_fixture['func']
    reduction_init = reduction_fixture['init']

    samples, features = X_train.shape
    reduction = reduction_func(**reduction_init)
    reduction.set_params(n_components=features)
    X_train_reduced = reduction.fit_transform(X_train, y_train)

    log("LDA reduced features to %d" % X_train_reduced.shape[1])

    # dimension_scatter(reduction_fixture, datasets, path)

    variance = []
    for component in range(1, features + 1):
        variance.append(reduction.explained_variance_ratio_[0:component].sum())

    plt.plot(range(1, features + 1), variance)
    plt.title('%s Cumulative Variance' % reduction_title)
    plt.xlabel('# of Components')
    plt.ylabel('Cumulative Variance')
    plt.grid(True)
    plt.xticks(range(1, features + 1), rotation=45)

    if __globals__["save"]:
        plt.savefig('results/%s/%s - Cumulative Variance.png' % (path, reduction_title))
    else:
        plt.show()

    plt.close()

    plt.plot(range(1, len(reduction.explained_variance_ratio_) + 1), reduction.explained_variance_ratio_)
    plt.title('%s Variance Ratio' % reduction_title)
    plt.xlabel('Components')
    plt.ylabel('Variance Ratio')
    plt.grid(True)
    plt.xticks(range(1, len(reduction.explained_variance_ratio_) + 1), rotation=45)

    if __globals__["save"]:
        plt.savefig('results/%s/%s - Variance Ratio.png' % (path, reduction_title))
    else:
        plt.show()

    plt.close()


def report_unsupervised_dimensionality_reduction(unsupervised_fixture, reduction_fixture, datasets, path):
    log("===============================================================")
    log("")

    X_train = datasets['X_train']
    y_train = datasets['y_train']
    X_test = datasets['X_test']
    y_test = datasets['y_test']

    samples, features = X_train.shape

    reduction_title = reduction_fixture['title']
    reduction_func = reduction_fixture['func']
    reduction_init = reduction_fixture['init']

    reduction = reduction_func(**reduction_init)

    unsupervised_title = unsupervised_fixture['title']
    unsupervised_func = unsupervised_fixture['func']
    unsupervised_init = unsupervised_fixture['init']

    log('Statistics for %s with %s' % (unsupervised_title, reduction_title))

    cluster = unsupervised_func(**unsupervised_init)

    X_train_reduced = reduction.fit_transform(X_train, y_train)

    fit_elapsed, cluster = timeit(cluster.fit, X_train_reduced)

    log("Training fit time: {}".format(format_time(fit_elapsed)))
    log("")

    train_predict_elapsed, y_train_pred = timeit(cluster.predict, X_train_reduced)

    log("Training query time: {}".format(format_time(train_predict_elapsed)))
    # log("Train adjusted rand index: {}".format(metrics.adjusted_rand_score(y_train, y_train_pred)))
    log("Train mutual information: {}".format(metrics.adjusted_mutual_info_score(y_train, y_train_pred)))
    # log("Train homogeneity: {}".format(metrics.homogeneity_score(y_train, y_train_pred)))
    # log("Train completeness: {}".format(metrics.completeness_score(y_train, y_train_pred)))
    # log("Train v-measure: {}".format(metrics.v_measure_score(y_train, y_train_pred)))
    # log("Train fowlkes mallows: {}".format(metrics.fowlkes_mallows_score(y_train, y_train_pred)))

    log("Train silhouette: {}".format(metrics.silhouette_score(X_train_reduced, y_train_pred)))

    log("")

    if X_train_reduced.shape[1] > 1:
        fig, ax = plt.subplots()

        fig.suptitle(unsupervised_title)

        min = X_train_reduced.min()
        max = X_train_reduced.max()

        if hasattr(cluster, 'cluster_centers_'):
            centers = cluster.cluster_centers_
        else:
            centers = cluster.means_

        ax.set_xlim([min, max])
        ax.set_ylim([min, max])
        ax.scatter(X_train_reduced[:, 0], X_train_reduced[:, 1], c=y_train_pred)
        ax.scatter(centers[:, 0], centers[:, 1], c='black', s=200, alpha=0.5)
        ax.grid()

        if __globals__["save"]:
            plt.savefig('results/%s/Cluster - %s with %s Training.png' % (path, unsupervised_title, reduction_title))
        else:
            plt.show()

        plt.close()

    X_test_reduced = reduction.transform(X_test)

    test_predict_elapsed, y_test_pred = timeit(cluster.predict, X_test_reduced)

    log("Test query time: {}".format(format_time(test_predict_elapsed)))
    # log("Test adjusted rand index: {}".format(metrics.adjusted_rand_score(y_test, y_test_pred)))
    log("Test mutual information: {}".format(metrics.adjusted_mutual_info_score(y_test, y_test_pred)))
    # log("Test homogeneity: {}".format(metrics.homogeneity_score(y_test, y_test_pred)))
    # log("Test completeness: {}".format(metrics.completeness_score(y_test, y_test_pred)))
    # log("Test v-measure: {}".format(metrics.v_measure_score(y_test, y_test_pred)))
    # log("Test fowlkes mallows: {}".format(metrics.fowlkes_mallows_score(y_test, y_test_pred)))

    log("Test silhouette: {}".format(metrics.silhouette_score(X_test_reduced, y_test_pred)))

    log("")


def report_supervised_dimensionality_reduction(supervised_fixture, reduction_fixture, datasets):
    log("===============================================================")
    log("")

    X_train = datasets['X_train']
    y_train = datasets['y_train']
    X_test = datasets['X_test']
    y_test = datasets['y_test']

    reduction_title = reduction_fixture['title']
    reduction_func = reduction_fixture['func']
    reduction_init = reduction_fixture['init']

    reduction = reduction_func(**reduction_init)

    supervised_title = supervised_fixture['title']
    supervised_func = supervised_fixture['func']
    supervised_init = supervised_fixture['init']

    log('Statistics for %s with %s' % (supervised_title, reduction_title))

    classifier = supervised_func(**supervised_init)

    X_train_reduced = reduction.fit_transform(X_train, y_train)

    fit_elapsed, classifier = timeit(classifier.fit, X_train_reduced, y_train)

    log("Training fit time: {}".format(format_time(fit_elapsed)))
    log("")

    train_predict_elapsed, y_train_pred = timeit(classifier.predict, X_train_reduced)

    log("Training query time: {}".format(format_time(train_predict_elapsed)))
    log("Train accuracy: {}".format(metrics.accuracy_score(y_train, y_train_pred)))
    log("Train mse: {}".format(metrics.mean_squared_error(y_train, y_train_pred)))
    log("")

    X_test_reduced = reduction.transform(X_test)

    test_predict_elapsed, y_test_pred = timeit(classifier.predict, X_test_reduced)

    log("Test query time: {}".format(format_time(test_predict_elapsed)))
    log("Test accuracy: {}".format(metrics.accuracy_score(y_test, y_test_pred)))
    log("Test mse: {}".format(metrics.mean_squared_error(y_test, y_test_pred)))
    log("")


def report_supervised_unsupervised_dimensionality_reduction(supervised_fixture, unsupervised_fixture, reduction_fixture, datasets):
    log("===============================================================")
    log("")

    X_train = datasets['X_train']
    y_train = datasets['y_train']
    X_test = datasets['X_test']
    y_test = datasets['y_test']

    reduction_title = reduction_fixture['title']
    reduction_func = reduction_fixture['func']
    reduction_init = reduction_fixture['init']

    reduction = reduction_func(**reduction_init)

    unsupervised_title = unsupervised_fixture['title']
    unsupervised_func = unsupervised_fixture['func']
    unsupervised_init = unsupervised_fixture['init']

    cluster = unsupervised_func(**unsupervised_init)

    X_train_reduced = reduction.fit_transform(X_train, y_train)

    fit_elapsed, cluster = timeit(cluster.fit, X_train_reduced)

    train_predict_elapsed, y_train_pred = timeit(cluster.predict, X_train_reduced)

    rows, columns = X_train_reduced.shape
    X_train_extended = np.zeros(shape=(rows, columns + 1))
    X_train_extended[:, 0:-1] = X_train_reduced
    X_train_extended[:, -1] = y_train_pred

    supervised_title = supervised_fixture['title']
    supervised_func = supervised_fixture['func']
    supervised_init = supervised_fixture['init']

    log('Statistics for %s with %s and %s' % (supervised_title, unsupervised_title, reduction_title))

    classifier = supervised_func(**supervised_init)

    fit_elapsed, classifier = timeit(classifier.fit, X_train_extended, y_train)

    log("Training fit time: {}".format(format_time(fit_elapsed)))
    log("")

    train_predict_elapsed, y_train_pred = timeit(classifier.predict, X_train_extended)

    log("Training query time: {}".format(format_time(train_predict_elapsed)))
    log("Train accuracy: {}".format(metrics.accuracy_score(y_train, y_train_pred)))
    log("Train mse: {}".format(metrics.mean_squared_error(y_train, y_train_pred)))
    log("")

    X_test_reduced = reduction.transform(X_test)

    train_predict_elapsed, y_test_pred = timeit(cluster.predict, X_test_reduced)

    rows, columns = X_test_reduced.shape
    X_test_extended = np.zeros(shape=(rows, columns + 1))
    X_test_extended[:, 0:-1] = X_test_reduced
    X_test_extended[:, -1] = y_test_pred

    test_predict_elapsed, y_test_pred = timeit(classifier.predict, X_test_extended)

    log("Test query time: {}".format(format_time(test_predict_elapsed)))
    log("Test accuracy: {}".format(metrics.accuracy_score(y_test, y_test_pred)))
    log("Test mse: {}".format(metrics.mean_squared_error(y_test, y_test_pred)))
    log("")


def report(supervised_fixtures, unsupervised_fixtures, reduction_fixtures, datasets, path):
    """
    Testing. In addition to implementing (wink), the algorithms described above, you should
    design two interesting classification problems. For the purposes of this assignment, a
    classification problem is just a set of training examples and a set of test examples. I
    don't care where you get the data. You can download some, take some from your own
    research, or make some up on your own. Be careful about the data you choose, though.
    You'll have to explain why they are interesting, use them in later assignments, and
    come to really care about them.
    :return:
    """

    # 1. Run the clustering algorithms on the data sets and describe what you see.
    for unsupervised_fixture in unsupervised_fixtures:
        report_unsupervised(unsupervised_fixture, datasets, path)

    # 2. Apply the dimensionality reduction algorithms to the two datasets and describe what you see.
    for reduction_fixture in reduction_fixtures:
        report_dimensionality_reduction(reduction_fixture, datasets, path)

    # 3. Reproduce your clustering experiments, but on the data after you've run dimensionality reduction on it.
    for unsupervised_fixture in unsupervised_fixtures:
        for reduction_fixture in reduction_fixtures:
            report_unsupervised_dimensionality_reduction(unsupervised_fixture, reduction_fixture, datasets, path)

    # X. Run the clustering algorithms on the data sets and describe what you see.
    for supervised_fixture in supervised_fixtures:
        report_supervised(supervised_fixture, datasets)

    # 4. Apply the dimensionality reduction algorithms to one of your datasets from assignment #1
    # (if you've reused the datasets from assignment #1 to do experiments 1-3 above then you've
    # already done this) and rerun your neural network learner on the newly projected data.
    for supervised_fixture in supervised_fixtures:
        for reduction_fixture in reduction_fixtures:
            report_supervised_dimensionality_reduction(supervised_fixture, reduction_fixture, datasets)

    # 5. Apply the clustering algorithms to the same dataset to which you just applied the
    # dimensionality reduction algorithms (you've probably already done this), treating the
    # clusters as if they were new features. In other words, treat the clustering algorithms as
    # if they were dimensionality reduction algorithms. Again, rerun your neural network learner
    # on the newly projected data.
    for supervised_fixture in supervised_fixtures:
        for unsupervised_fixture in unsupervised_fixtures:
            for reduction_fixture in reduction_fixtures:
                report_supervised_unsupervised_dimensionality_reduction(supervised_fixture, unsupervised_fixture, reduction_fixture, datasets)


def grid_search(pipe, params, X, y):
    grid_search_cv = GridSearchCV(
        pipe,
        param_grid=params,
        cv=__globals__["cv"],
        n_jobs=-1,  # run in parallel
        verbose=10,
        # scoring=metrics.make_scorer(scorer)
    )
    return grid_search_cv.fit(X, y)


def run_grid_search(estimator_fixture, reduction_fixture, datasets, path):
    estimator_title = estimator_fixture['title']
    estimator_func = estimator_fixture['func']
    estimator_init = estimator_fixture['init']
    estimator_grid_search = estimator_fixture['grid_search']

    reduction_title = reduction_fixture['title']
    reduction_func = reduction_fixture['func']
    reduction_init = reduction_fixture['init']
    reduction_grid_search = reduction_fixture['grid_search']

    log(estimator_title + ' with ' + reduction_title)

    X_train = datasets['X_train']
    y_train = datasets['y_train']

    pipe = Pipeline([('reduction', reduction_func(**reduction_init)), ('estimator', estimator_func(**estimator_init))])

    grid_search_params = estimator_grid_search.copy()
    grid_search_params.update(reduction_grid_search.copy())

    # Run the search grid on all of the training data
    elapsed, search = timeit(grid_search, pipe, grid_search_params, X_train, y_train)
    log("Time to find best hyperparameters: {}".format(format_time(elapsed)))
    log("Best params: {}".format(search.best_params_))
    log("Best score: {}".format(search.best_score_))
    log("")

    tmp = pd.DataFrame(search.cv_results_)
    tmp.to_csv('results/' + path + '/' + estimator_title + ' - ' + reduction_title + '.csv')


def run(path,
        target,
        supervised_fixtures=[],
        unsupervised_fixtures=[],
        reduction_fixtures=[],
        separator=',',
        preprocess=None):

    if __globals__["grid_search"]:
        open_log(path, "GridSearch.txt")
    else:
        open_log(path, "Report.txt")

    datasets = load_datasets(path, target, preprocess=preprocess, separator=separator, reset=__globals__["reset"])

    if __globals__["grid_search"]:
        for unsupervised_fixture in unsupervised_fixtures:
            for reduction_fixture in reduction_fixtures:
                run_grid_search(unsupervised_fixture, reduction_fixture, datasets, path)
    else:
        report(supervised_fixtures, unsupervised_fixtures, reduction_fixtures, datasets, path)

    close_log()