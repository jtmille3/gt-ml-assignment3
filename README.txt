### Installation and Execution Instructions
1. Borrowed from: http://quantsoftware.gatech.edu/ML4T_Software_Installation
2. Install virtualenv (if it is not already installed): python -m pip install --user virtualenv
3. Create a viritual environment for this class: virtualenv assignment1-venv
4. Activate the new virtual environment: source assignment3-venv/bin/activate on Linux or macOS, ml4t-venv/Scripts/activate.bat on Windows.
5. Install the libraries using the requirements file you just saved: python -m pip install --requirement assignment3-libraries.txt
6. Run individual test fixtures: 

    > pytest test_adult_census_income.py
    
    > pytest test_winequality-red.py
    
### Datasets   
[Wine Quality - Red Dataset](https://archive.ics.uci.edu/ml/machine-learning-databases/wine-quality/)
[Adult Census Income](https://archive.ics.uci.edu/ml/machine-learning-databases/adult/)

### Code
[Assignment 3](https://gitlab.com/jtmille3/gt-ml-assignment3)
